__version__='0.1.7'

from .tps_io import TPSImage, TPSPoints, TPSCurve, TPSFile
from . import tps_io