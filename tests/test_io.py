import inspect
from py_tps import tps_io
import os
import shutil
import tempfile

import numpy as np
import pytest
from numpy import testing as np_test


def data_path(input_file_name, decrement=1):
    # what file is this called from?
    stack = inspect.stack()
    file_name = inspect.getfile(stack[decrement][0])
    file_directory = os.path.dirname(file_name)

    base_path = os.path.join(file_directory, 'test_data', input_file_name)
    test_data_path = os.path.normpath(base_path).replace('\\', '/')
    return test_data_path

@pytest.yield_fixture(scope='function', params=None, autouse=False)
def temporary_directory():
    output_directory = tempfile.mkdtemp('output')
    yield output_directory
    shutil.rmtree(output_directory)

def test_read_files(temporary_directory):
    in_path = data_path('Test_1.TPS')

    tps_file = tps_io.TPSFile.read_file(in_path)

    assert len(tps_file.images) == 1
    assert isinstance(tps_file.images[0], tps_io.TPSImage)
    assert isinstance(tps_file.images[0].landmarks, tps_io.TPSPoints)
    assert isinstance(tps_file.images[0].scale, float)
    assert tps_file.images[0].curves is None
    assert tps_file.images[0].image == 'T1.JPG'
    assert tps_file.images[0].id_number == 0
    np_test.assert_almost_equal(tps_file.images[0].scale, 0.004584)

    expected_arr = np.array([[1872.00000, 1876.00000], [1915.00000, 1966.00000],
                             [1958.00000, 2055.00000], [1940.00000, 2138.00000]])

    np_test.assert_array_almost_equal(tps_file.images[0].landmarks.points, expected_arr)

    out_file = os.path.join(temporary_directory, 'Test1_Out.TPS')
    tps_file.write_to_file(out_file)
    tps_file_new = tps_io.TPSFile.read_file(out_file)

    assert len(tps_file_new.images) == 1
    assert isinstance(tps_file_new.images[0], tps_io.TPSImage)
    assert isinstance(tps_file_new.images[0].landmarks, tps_io.TPSPoints)
    assert isinstance(tps_file_new.images[0].scale, float)
    assert tps_file_new.images[0].curves is None
    assert tps_file_new.images[0].image == 'T1.JPG'
    assert tps_file_new.images[0].id_number == 0
    np_test.assert_almost_equal(tps_file_new.images[0].scale, 0.004584)


    np_test.assert_array_almost_equal(tps_file_new.images[0].landmarks.points, expected_arr)

def test_read_files_2():
    in_path = data_path('Test_2.TPS')

    tps_file = tps_io.TPSFile.read_file(in_path)

    assert len(tps_file.images) == 2
    assert tps_file.images[0].image == 'T1.JPG'
    assert tps_file.images[0].id_number == 0
    assert tps_file.images[0].comment is None
    assert tps_file.images[1].image == 'T2.JPG'
    assert tps_file.images[1].id_number == 1
    assert tps_file.images[1].comment == 'WOO'
    np_test.assert_almost_equal(tps_file.images[0].scale, 0.004584)
    np_test.assert_almost_equal(tps_file.images[1].scale, 0.1)

    expected_arr = np.array([[1872.00000, 1876.00000], [1915.00000, 1966.00000],
                             [1958.00000, 2055.00000], [1940.00000, 2138.00000]])

    np_test.assert_array_almost_equal(tps_file.images[0].landmarks.points, expected_arr)

    expected_arr2 = np.array([[1872.00000, 1876.00000], [1915.00000, 1966.00000],
                              [1958.00000, 2055.00000],[1958.00000, 2055.00000], [1940.00000, 2138.00000]])
    np_test.assert_array_almost_equal(tps_file.images[1].landmarks.points, expected_arr2)


def test_read_files_3(temporary_directory):
    in_path = data_path('Test_3.TPS')

    tps_file = tps_io.TPSFile.read_file(in_path)
    assert isinstance(tps_file.images[0].landmarks, tps_io.TPSPoints)
    assert len(tps_file.images[0].curves) == 2
    assert isinstance(tps_file.images[0].curves[0], tps_io.TPSCurve)
    assert tps_file.images[0].image == 'T2.JPG'
    assert tps_file.images[0].id_number == 1
    assert tps_file.images[0].comment == 'WOO'
    np_test.assert_almost_equal(tps_file.images[0].scale, 0.1)

    expected_arr = np.array([[1872.00000, 1876.00000], [1915.00000, 1966.00000],
                             [1958.00000, 2055.00000], [1940.00000, 2138.00000]])

    np_test.assert_array_almost_equal(tps_file.images[0].curves[0].tps_points.points, expected_arr)

    expected_arr2 = np.array([[1872.00000, 1876.00000], [1915.00000, 1966.00000],
                              [1958.00000, 2055.00000],[1958.00000, 2055.00000], [1940.00000, 2138.00000]])
    np_test.assert_array_almost_equal(tps_file.images[0].curves[1].tps_points.points, expected_arr2)


    out_file = os.path.join(temporary_directory, 'Test3_Out.TPS')
    tps_file.write_to_file(out_file)
    tps_file_new = tps_io.TPSFile.read_file(out_file)
    assert isinstance(tps_file_new.images[0].landmarks, tps_io.TPSPoints)
    assert len(tps_file_new.images[0].curves) == 2
    assert isinstance(tps_file_new.images[0].curves[0], tps_io.TPSCurve)
    assert tps_file_new.images[0].image == 'T2.JPG'
    assert tps_file_new.images[0].id_number == 1
    assert tps_file_new.images[0].comment == 'WOO'
    np_test.assert_almost_equal(tps_file_new.images[0].scale, 0.1)

    np_test.assert_array_almost_equal(tps_file_new.images[0].curves[0].tps_points.points, expected_arr)

    np_test.assert_array_almost_equal(tps_file_new.images[0].curves[1].tps_points.points, expected_arr2)


if __name__ == '__main__':
    test_read_files()
